# Tic Tac Toe, czyli kółko i krzyżyk
W tym repozytorium znajdziesz kod związany z aplikacją konsolową do gry w kółko i krzyżyk w Java.
Kod ten robimy w ramach projektu podsumowującego typy prymitywne, tablice i instrukcje sterujące.

Tworząc go zakładamy, że nie znasz jeszcze podstaw **programowania obiektowego**. Z góry uprzedzamy
falę **seniorskich** uwag. :)